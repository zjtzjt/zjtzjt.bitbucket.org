#!/usr/bin/env perl

my %files;
use File::Basename;
use Cwd;
while ( <> ) {
  chomp;
  my $dir = dirname $_;
  my $file = basename $_;
  $files{$dir}{$file}++;
}
my $origdir = getcwd;
for my $dir ( keys %files ) {
#  print "DIR $dir\n";
  chdir $dir;
  for my $file ( keys %{$files{$dir}} ) {
    my $cwd = getcwd;
    print "CWD $cwd\n\n";
    if ( -e $file ) {
      my ($filenoext, $ext) = $file =~ /(.*)\.(...)$/;
      if ( $ext eq 'm4a' ) {
        print "RUNNING soundconverter -b $file\n\n";
        system "soundconverter", "-b", $file;
        if ( -e $file and -e "$filenoext.ogg" ) {
          print "RUNNING unlink $file\n\n";
          unlink $file;
        }
      }
    }
    else {
      print "   NONONON $cwd/$file\n";
    }
  }
  chdir $origdir;
}
